function isNonEmptyString(v: unknown): boolean {
	if (v === null || v === undefined) {
		return false
	}
	if (typeof v === "string") {
		return v.length > 0
	}
	return false
}

export interface Config {
	gitlabBotAccessToken: string
	gitlabUrl: string
}

export function loadConfigFromEnv(): Config {
	const gitlabBotAccessToken = import.meta.env.VITE_GITLAB_BOT_ACCESS_TOKEN as string
	if (!isNonEmptyString(gitlabBotAccessToken)) {
		throw new Error(`VITE_GITLAB_BOT_ACCESS_TOKEN environment variable must be a non-empty string`)
	}

	const gitlabUrl = "https://gitlab.com"

	return {
		gitlabBotAccessToken,
		gitlabUrl,
	}
}
