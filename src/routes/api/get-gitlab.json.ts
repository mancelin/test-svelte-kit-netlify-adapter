export const get = async (request) => {
	const { gitlabBotAccessToken, gitlabUrl } = request.locals.config

  return {
    body: {
      host: gitlabUrl,
		  oauthToken: gitlabBotAccessToken,
    }
  }
}
