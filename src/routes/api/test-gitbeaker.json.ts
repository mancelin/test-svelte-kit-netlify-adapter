import { Gitlab } from "@gitbeaker/node"

export const get = async (request) => {
	const projectPathWithNamespace = request.query.get("project")

	const { gitlabBotAccessToken, gitlabUrl } = request.locals.config

	const gitlabClient = new Gitlab({
		host: gitlabUrl,
		oauthToken: gitlabBotAccessToken,
	})

	const { Projects } = gitlabClient

	const project = await Projects.show(projectPathWithNamespace)

  return {
		body: {
			project: project,
		},
	}
}
